module.exports={
//define entry point
entry:'./index.js',

//define output
output:{
    path:'F:\\webpack-playlist\\dist',
    filename:'bundel.js'
},

module:{
 rules:
 [
    {
        test:/\.js$/,
        exclude:/(node_modules)/,
        loader:'babel-loader',
        query:
        {
            presets:['@babel/preset-env']
        }
    }
 ]
}
};